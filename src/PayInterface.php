<?php
namespace KukePay;
use KukePay\Tencent\Application;
use KukePay\Tencent\Kernel\SignFactory;

/**
 * @method static Application    tencent(array $config)
 * @method static \KukePay\Ali\Application ali(array $config)
 * @package KukePay
 */
class PayInterface
{

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    private static function make($name, $arguments)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\{$namespace}\\Application";
        return new $class(...$arguments);
    }

    static function __callStatic($name, $arguments)
    {
        return self::make($name,$arguments);
    }
}