<?php
namespace KukePay\Tencent;
use KukePay\Tencent\Kernel\SignFactory;
use KukePay\Tencent\Native\Factory;

/**
 * @property Factory $native
 * @property \KukePay\Tencent\JsApi\Factory $jsapi
 * @property \KukePay\Tencent\OAuth\Factory $oauth
 * @package KukePay\Tencent
 */
class Application
{
    private $config;
    /**
     * Application constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * make new application
     * @param $name
     * @return mixed
     */
    private function make($name)
    {
        $namespace = ucfirst($name);
        $class = "KukePay\\Tencent\\{$namespace}\\Factory";
        return new $class($this->config);
    }

    public function __get($name)
    {
        return $this->make($name);
    }

}