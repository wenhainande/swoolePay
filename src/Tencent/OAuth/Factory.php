<?php

namespace KukePay\Tencent\OAuth;

use KukePay\ExceptionHandler;
use KukePay\Tencent\Kernel\RequestFactory;

/**
 * 微信公众号授权
 * @package KukePay\Tencent\Native
 */
class Factory
{
    private $config;
    private $scopes;
    private $request;
    /**
     * Factory constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->request = RequestFactory::getInterface();
    }

    /**
     * 设置授权模式
     * @param $scopes
     * @return $this
     */
    public function setScopes($scopes)
    {
        $this->scopes = $scopes;
        return $this;
    }

    /**
     * 获取授权地址
     * @param string $redirect
     * @return string
     */
    public function redirect(string $redirect)
    {
        return 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$this->config['app_id'].'&redirect_uri='.urlencode($redirect).'&response_type=code&scope='.$this->scopes.'&state=STATE#wechat_redirect';
    }

    /**
     * 获取用户信息
     * @param string $code
     * @return mixed|string
     */
    public function getUserInfo(string $code)
    {
        try{
            if (!isset($code)) throw new ExceptionHandler('can not be empty');

            if (!isset($this->scopes)) throw new ExceptionHandler('Please set up scopes))');

            $uri = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$this->config['app_id'].'&secret='.$this->config['secret'].'&code='.$code.'&grant_type=authorization_code';

            $accessToken = $this->request->client->get($uri)->getBody()->getContents();

            $accessToken = json_decode($accessToken,true);

            if (!isset($accessToken['access_token']) || !isset($accessToken['openid'])) throw new ExceptionHandler($accessToken['errmsg']);

            if ($this->scopes == 'snsapi_base') return $accessToken;

            $get_user_info_url = "https://api.weixin.qq.com/sns/userinfo?access_token={$accessToken['access_token']}&openid={$accessToken['openid']}&lang=zh_CN";

            $userInfo = $this->request->client->get($get_user_info_url)->getBody()->getContents();

            return json_decode($userInfo,true);

        }catch (ExceptionHandler $exception){
            return $exception->getMessage();
        }
    }

}