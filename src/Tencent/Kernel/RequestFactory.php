<?php


namespace KukePay\Tencent\Kernel;


use GuzzleHttp\Client;

class RequestFactory
{
    private static $interface;
    public $client;
    private function __construct(){
        $this->client = new Client();
    }
    private function __clone(){}

    /**
     * @return RequestFactory
     */
    public static function getInterface()
    {
        if (!isset(self::$interface)){
            self::$interface = new self();
        }
        return self::$interface;
    }
}