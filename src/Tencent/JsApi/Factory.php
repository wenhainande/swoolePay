<?php

namespace KukePay\Tencent\JsApi;

use KukePay\Tencent\Unifiedorder\Application;

/**
 * JSAPI支付
 * @package KukePay\Tencent\Native
 */
class Factory
{
    private $config;
    /**
     * Factory constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * 发起支付
     * @param array $params
     * @return array|string
     */
    public function send(array $params)
    {
        return Application::getInterface($this->config)->send($params,'JSAPI',function ($response,Application $obj){
            return $obj->getParams($response);
        });
    }
}