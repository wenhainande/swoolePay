# 欢迎使用swoolePay

**swoolePay 基于swoole协程而生 不依赖任何第三方框架 使用一键curl协程即可实现协程化**

## 安装

> composer require kuke/swoole-pay

## 微信

#####扫码支付
```php
 * 扫码支付
 */
include_once dirname(__DIR__) . '/vendor/autoload.php';

$tencent = \KukePay\PayInterface::tencent([
    'app_id' => '',
    'mch_id'=>'',
    'key'=>''
]);

$array = $tencent->native->send([
    'body'  =>  '测试购买',
    'spbill_create_ip'  =>  '127.0.0.1',
    'total_fee'  =>  0.01*100,
    'out_trade_no'=>uniqid(),
    'notify_url'=>'http://www.baidu.com'
]);

echo "<img src='http://qr.liantu.com/api.php?text={$array['code_url']}'>";
```

#####jsapi支付 适用于 h5/小程序/公众号
```php
<?php
/**
 * JsApi支付
 */
include_once dirname(__DIR__) . '/vendor/autoload.php';

$tencent = \KukePay\PayInterface::tencent([
    'app_id' => '',
    'mch_id'=>'',
    'key'=>''
]);

$array = $tencent->jsapi->send([
    'body'  =>  '测试购买',
    'spbill_create_ip'  =>  '127.0.0.1',
    'total_fee'  =>  0.01*100,
    'out_trade_no'=>uniqid(),
    'notify_url'=>'http://www.baidu.com',
    'openid'=>'oVz_s4sSex8DvPRaTyIoQlwVidCE'
]);
echo json_encode($array);
```
#####公众号授权
```php
<?php
/**
 * 扫码支付
 */
include_once dirname(__DIR__) . '/vendor/autoload.php';

$tencent = \KukePay\PayInterface::tencent([
    'app_id' => '',
    'secret'=>''
]);

function getUri()
{
    $uri = $_SERVER['REQUEST_URI'];

    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ?

        "https://": "http://";



    $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

    return $url;//输出完整的url
}

$oauth = $tencent->oauth;

$uri = $oauth->setScopes('snsapi_userinfo')->redirect(getUri());


if (empty($_GET['code'])){
    header('location:'.$uri);die;
}

echo '<pre>';

var_dump($oauth->getUserInfo($_GET['code']));
```
#### 未完成功能
	支付退款
	订单查询
	企业付款(暂时没资质)
	后续会将 微信公众号/小程序大部分常用的servier api接入...

### 微信全局配置

| key        | 名称   | 
| --------   | -----:  |
| app_id      | 微信appid   |
| secret     |   微信secret   | 
| mch_id        |    商户mch_id    |
| key        |    商户key    |

## 支付宝 进行中...

### 支付宝全局配置
| key        | 名称   | 
| --------   | -----:  |
| app_id      | 应用id   |
| rsaPrivateKey     |   私钥   | 
| signType        |    RSA2    |

###注：
    swoolePay为什么不使用证书加密方式?
        1：我们的设计是用在前后端分离的模式上，因为支付宝或者微信使用证书可以直接退款，为了安全性着想这块实现还是交给开发者自行实现！
        
    swoolePay能为开发者快速完成微信和支付宝支付,统一返回结果让开发者自由处理，我们只负责提交和处理返回