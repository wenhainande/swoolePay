<?php
/**
 * 扫码支付
 */
include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';

$ali = \KukePay\PayInterface::ali([
    'app_id' => '',
    'rsaPrivateKey' => '',
    'signType' => 'RSA2'
]);


var_dump($ali->native->send([
    'out_trade_no' => uniqid(),
    'total_amount' => 0.01, //单位 元
    'subject' => '测试支付',  //订单标题
    'timeout_express' => '2h',       //该笔订单允许的最晚付款时间，逾期将关闭交易。取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为 90m。
    'notify_url'=>'http://www.baidu.com'
]));