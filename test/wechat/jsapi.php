<?php
/**
 * JsApi支付
 */
include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';

$tencent = \KukePay\PayInterface::tencent([
    'app_id' => '',
    'mch_id'=>'',
    'key'=>''
]);

$array = $tencent->jsapi->send([
    'body'  =>  '测试购买',
    'spbill_create_ip'  =>  '127.0.0.1',
    'total_fee'  =>  0.01*100,
    'out_trade_no'=>uniqid(),
    'notify_url'=>'http://www.baidu.com',
    'openid'=>'oVz_s4sSex8DvPRaTyIoQlwVidCE'
]);
echo json_encode($array);