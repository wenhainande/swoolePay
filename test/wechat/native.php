<?php
/**
 * 扫码支付
 */
include_once dirname(dirname(__DIR__)) . '/vendor/autoload.php';

$tencent = \KukePay\PayInterface::tencent([
    'app_id' => '',
    'mch_id'=>'',
    'key'=>''
]);

Co::set(['hook_flags'=> SWOOLE_HOOK_ALL]);
//高性能HTTP服务器
$http = new Swoole\Http\Server("127.0.0.1", 9501);

$http->on("request", function ($request, $response)use($tencent) {
    $wg = new \Swoole\Coroutine\WaitGroup();
    $result = [];
    for ($i = 0;$i < 5;$i++){
        $wg->add();
        go(function ()use ($tencent,$wg,$i,&$result){
            $array = $tencent->native->send([
                'body'  =>  '测试购买',
                'spbill_create_ip'  =>  '127.0.0.1',
                'total_fee'  =>  0.01*100,
                'out_trade_no'=>uniqid(),
                'notify_url'=>'http://www.baidu.com'
            ]);
            $array['index'] = $i;
            array_push($result,$array);
            $wg->done();
        });
    }
    $wg->wait();
    $_html = '';
    foreach ($result as $key=>$value){
        $_html .= "<div><img src='http://qr.liantu.com/api.php?text={$value['code_url']}'><p>$key</p></div>";
    }
    $response->end($_html);

});

$http->start();

//echo "<img src='http://qr.liantu.com/api.php?text={$array['code_url']}'>";